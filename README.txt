############################################################################################################
# This is the README written on the delivery day.                                                           # 
# All points are covered                                                                                    #
# It is an assigment for a screening test                                                                   #
# Refer the tasks on assignment email                                                                       #
# It might be emotional expressions on the file  but don't bother about them                                #
# Here is the solution                                                                                      #
############################################################################################################



1. Install Ubuntu with provided iso image
2. To make ssh available on the server
    # sudo apt update
    # sudo apt install openssh-server
    # sudo systemctl status ssh
    # ssh localhost
3. Upgrade to the latest version of 16.4
    # sudo apt-get update
    # sudo apt-get upgrade
    # sudo apt-get upgrade --> just to make sure all are done
4. The following link is the right one: 
    http://127.0.0.1:8080/demo/go-web-hello-world
4a. Build the myapp application from the go-web-hello-world: using the following command: go build -o myapp
    Make sure there is only one go script in the directory otherwise use the script name as reference, or put a source code file
5. Myapp is running as a service put under /etc/init.d 
   Put the myapp in the following directory: /etc/init.d
   run the following command sudo update-rc.d myapp  defaults.
6. Run Docker go-hello2 as service with the following script.
   So the hello world will run as service.
        #!/bin/bash
         sudo docker run -d -p 8083:8083 go-hello2
   It is named as run-docker-as-a-service.sh
   This port 8083 naming might not needed, but I am tired of doing more testing 
   so let it be.
   Put the script /etc/init.d
   run the following command:  sudo update-rc.d run-docker-as-a-service.sh default
6a. The task is asking for 8082 as a port, but the 8082 port is used by docker internal so we can't used 8082. Instead we use 8083.....Horray !!!!!!
7. Just to make life easier, reboot the server when those 2 services defined.
8. To make sure those services are running: do the following:
   http://127.0.0.1:8081 and http://127.0.0.1:8083
   It will show: Go Web  Hello World ! on  your web browser
NOTES: ON Kubernetes, I have a very limited knowledge, I ask people, read etc
so there is no way, it is an ultimate correct way, but at least it show a kind of my determination to get it done. Hopefully good enough. I might still update the 1,2 files later today on Friday. 
9. install a single node Kubernetes cluster using kubeadm
   admin.conf can be found 
   In this installation, it wasn't that easy: swap file one of them, K8 didn't like it
10. deploy the hello world container into the kubernetes above and expose the service to nodePort 31080
    Port forwarding is the biggest issue.
11. Install kubernetes dashboard and expose the service to nodeport
    the same issue as 10
12. figure out how to generate token to login to the Dashboard and publish the procedure to the gitlab.
    Follow this instruction https://www.replex.io/blog/how-to-install-access-and-add-heapster-metrics-to-the-kubernetes-dashboard
--
Create a service account
When the service account is created, a secret is created with the service account
"Kubectl describe ServiceAccount <accountname>" shows the secret name
To see  the secret with "kubectl describe secret <secretname>"
Add permissions to that service account
"kubectl create clusterrolebinding <bindingname> --clusterrole=cluster-admin --serviceaccount=default:<accountname>"
---
Follow this instruction https://www.replex.io/blog/how-to-install-access-and-add-heapster-metrics-to-the-kubernetes-dashboard



NOTE: 
Docker hub account:
https://hub.docker.com/repository/docker/godfriedal/go-web-hello-world


Not sure this is good enough, but this the best I can gave, having spending until late nights every day for 4 days in a row. 

Thanks though for the learning !!!!


